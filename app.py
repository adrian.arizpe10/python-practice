# name = input('What is your name?')
# color = input('What is your favorite color?')
# print(name + ' likes the color ' + color)
#
# birth_year = input('Birth year: ')
# print(type(birth_year))
# age = 2020 - int(birth_year)
# print(type(age))
# print(age)
#
# weight = input('What is your weight?')
# print(int(weight) * 0.45)

# course = 'Python for Beginners'
# another = course[:]
# print(another)

# email = '''
# Hi John,
#
# This is our first email
#
# Thank you,
# The Support Team
# '''
# print(email)


# name = 'Jeniffer'
# print(name[1:-1])

# first = 'John'
# last = 'Smith'
# message = first + '[' + last + '] is a coder'
# msg = f'{first} [{last}] is a coder'
# print(message)
# print(msg)

# course = 'Python for Beginners'
# print(course.find('Beginners'))
# print(course.replace('P', 'J'))
# print('Python' in course)
# len()
# course.upper()
# course.lower()
# course.find()
# course.replace()
# '...' int course

# first = input('What is your name?')
# print('hi ' + first)

# Youtube
# from  __future__ import unicode_literals
# import youtube_dl
# import os
# #
# class Download(object):
#     def __init__(self, url):
#         self.url = url
#         self.save_path = os.path.join(os.path.expanduser('~'), 'Downloads')
#         self.song()
#
#         def song(self):
#             opts = {
#                 'verbose': True,
#                 'fixup': 'detect_or_warn',
#                 'format': 'bestaudio/best',
#                 'postprocessors':[{
#                     'key': 'FFmpegExtractAudio',
#                     'preferredcodec': 'mp3',
#                     'preferredquality': '1411',
#                 }],
#                 'extractaudio': True,
#                 'outtmpl': self.save_path + '/%(title)s.%(ext)s',
#                 'noplaylist': True
#             }
#             ydl = youtube_dl.YoutubeDL(opts)
#             ydl.download([self.url])
#             if __name__ == '__main__':
#                 url = input("Enter url to song here\n >> ")
#                 Download.url

# print(10 ** 3)

# x = 10
# x  %= 3
# print(x)

# import math
# x = 2.9
# print(math.floor(2.9)
# )

# is_hot = False
# is_cold = False
#
# if is_hot:
#     print("It's a hot day")
#     print("Drink plenty of water")
# elif is_cold:
#     print("It's a cold day")
#     print("Wear warm clothes")
# else:
#     print("It's a lovely day")

# house_one_million = 1000000
# good_credit = True
# if good_credit:
#     result = int(house_one_million) * .10
# else:
#     result = int(house_one_million) * .20
# print(f"Down payment: {result}")

# good_credit = True
# has_criminal_record = True
# if good_credit and not has_criminal_record:
#     print("Eligible for loan")

# temperature = 35
# if temperature > 30:
#     print("its a hot day")
# else:
#     print("Its not a hot day")

# name = "J"
# if len(name) < 4:
#     print("Name must be at least 3 characters")
# elif len(name)> 50:
#     print("Name must be a maximum of 50 characters")
# else:
#     print("Looks Good!")

# weight = int(input("Weight: "))
# unit = input("(L)bs or (K)g: ")
# if unit.upper() == "K":
#     converted = weight * 0.45
#     print(f"Your are {converted} kilos")
# else:
#     converted = weight / 0.45
#     print(f"Your are {converted} pounds")

# i = 1
# while i <= 5:
#     print('*' * i)
#     i = i + 1
# print("Done!")

# secret_number = 9
# guess_count = 0
# guess_limit = 3
# while guess_count < guess_limit:
#     guess = int(input('Guess: '))
#     guess_count += 1
#     if guess == secret_number:
#         print('You Won!')
#         break
# else:
#     print('Sorry, you failed!')

# command = ""
# started = False
# while True:
#     command = input(">").lower()
#     if command == "start":
#         if started:
#             print("Car is already started!")
#         else:
#             started = True
#             print("Car started....Ready to go!")
#     elif command == "stop":
#         if not started:
#             print("Car is already stopped!")
#         else:
#             started = False
#             print("Car stopped.")
#     elif command == "quit":
#         print("Exiting....")
#     elif command == "help":
#         print('''
# type:
# start - to start the car
# stop - to stop the car
# quit - to exit
#         ''')
#     elif command == "quit":
#         break
#     else:
#         print("Sorry I don't understand that...")
#

# for item in range(10):
#     print(item)

# prices = [10, 20, 30]
# total = 0
# for price in prices:
#     total += price
# print(f"Total: {total}")

# (x, y)
# (0, 0)
# (0, 1)
# (0, 2)
# (1, 0)
# (1, 1)
# (1, 2)
# for x in range(4):
#     for y in range(3):
#         print(f"({x},{y})")

# numbers = [5, 2, 5, 2, 2]
# for x_count in numbers:
#     output = ''
#     for count in range(x_count):
#         output += 'x'
#     print(output)

# names = ['John', 'Bob', 'Mosh', 'Sarah', 'Mary']
# names[0] = 'Jon'
# print(names[2:])
# print(names)

# numbers = [1, 2, 3, 4, 5, 6, 7]
# max = numbers[0]
#
# for number in numbers:
#     if number > max:
#         max = number
# print(max)

# [
#     123
#     456
#     789
# ]
# matrix = [
#     [1,2,3],
#     [4,5,6],
#     [7,8,9]
# ]
# for row in matrix:
#     for item in row:
#         print(item)

# numbers = [5, 2, 1, 7, 4]
# numbers2 = numbers.copy()
# numbers.insert(0, 10)
# numbers.remove(5)
# numbers.pop()
# print(numbers.count(5))
# print(numbers.sort())
# numbers.reverse()
# print(numbers)
# print(50 in numbers)
# numbers.clear()
# print(numbers)
# print(numbers2)

# numbers = [3, 4, 2, 5, 6, 2, 4, 5, 3, 4, 6, 8]
# key = numbers[0]
# for number in numbers:
#     for number in numbers:
#         if number == key:
#             numbers.remove(key)
#             key = number
# print(numbers)

# uniques = []
# for number in numbers:
#   if number not in uniques:
#       uniques.append(number)
# print(uniques)

# numbers = (1, 2, 3)
# print(numbers[0])

# coordinates = [1, 2, 3]
# x, y, z = coordinates
# print(y)

# customer = {
#     "name": "John Smith",
#     "Age": 30,
#     "is_verified": True
# }
# customer["name"] = "Jack Smith"
# print(customer["name"])
# print(customer.get("birthdate", "Jan 1 1980"))


# phone = input("Phone:")
# digits_mapping = {
#     "1": "One",
#     "2": "Two",
#     "3": "Three",
#     "4": "Four"
# }
# output = ""
# for ch in phone:
#     output += digits_mapping.get(ch, "!") + " "
# print(output)

# message = input(">")
# words = message.split(' ')
# emojies = {
#     ":)": "🙂",
#     ":(": "😞",
#     ":D": "😄",
#     ":o": "😵",
#     "thumbs-up": "👍",
#     "thumbs-down": "👎",
#     "alien": "👽",
#     "star-eyes": "🤩",
#     "puppy-eyes": "🥺"
# }
# output = ""
# for word in words:
#     output += emojies.get(word, word) + " "
# print(output)

# def greet_user(first_name, last_name):
#     print(f'Hi {first_name} {last_name}!')
#     print('Welcome Aboard!')
#
#
# print("Start")
# greet_user(last_name="Smith", first_name="John")
# calc_cost(total=50, shipping=5, discount=0.1)
# print("Finish")

# def square(number):
#      print(number * number)
#
# print(square(3))


# def emoji_converter(message):
#     words = message.split(' ')
#     emojies = {
#         ":)": "🙂",
#         ":(": "😞",
#         ":D": "😄",
#         ":o": "😵",
#         "thumbs-up": "👍",
#         "thumbs-down": "👎",
#         "alien": "👽",
#         "star-eyes": "🤩",
#         "puppy-eyes": "🥺"
#     }
#     output = ""
#     for word in words:
#         output += emojies.get(word, word) + " "
#     return output
#
#
# message = input(">")
# print(emoji_converter(message))

# try:
#     age = int(input('Age: '))
#     income = 20000
#     risk = income / age
#     print(age)
# except ZeroDivisionError:
#     print("Age cannot be zero")
# except ValueError:
#     print("invalid value")

